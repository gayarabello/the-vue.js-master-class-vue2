import Vue from 'vue'

import Router from 'vue-router'

import Home from "@/pages/PageHome";
import ThreadShow from "@/pages/PageThreadShow";
import Forum from "@/pages/PageForum";
import NotFound from "@/pages/NotFound";
import Category from "@/pages/PageCategory";

Vue.use(Router)

const router = new Router({
    routes:
        [
            {
                path: '/',
                name: 'Home',
                component: Home,
            },
            {
                path: '/thread/:id',
                name: 'ThreadShow',
                component: ThreadShow,
                props: true,
            },
            {
                path: '/:pathMatch(.*)*',
                name: 'NotFound',
                component: NotFound,
            },
            {
                path: '/category/:id',
                name: 'category',
                component: Category,
                props: true,
            },
            {
                path: '/forum/:id',
                name: 'forum',
                component: Forum,
                props: true,
            }
        ]
})


export default router;